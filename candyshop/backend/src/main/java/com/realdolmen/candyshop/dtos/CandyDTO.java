
package com.realdolmen.candyshop.dtos;

import com.realdolmen.candyshop.domain.Color;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author SDOAX36
 */
@XmlRootElement(name = "candy")
@XmlAccessorType(XmlAccessType.FIELD)
public class CandyDTO {
    
    @XmlAttribute(required = false)
    private Long id;
    
    @XmlElement(required=true)
    private String color;
    @XmlElement(required = true)
    private String name;
    @XmlElement(required = true)
    private int price;

    public CandyDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    
}   
   
