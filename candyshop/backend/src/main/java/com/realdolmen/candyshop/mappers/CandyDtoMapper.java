
package com.realdolmen.candyshop.mappers;

import com.realdolmen.candyshop.builders.CandyBuilder;
import com.realdolmen.candyshop.domain.Candy;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.dtos.CandyDTO;
import java.util.function.Function;

/**
 *
 * @author SDOAX36
 */
public class CandyDtoMapper implements Function<CandyDTO, Candy>{

    @Override
    public Candy apply(CandyDTO t) {
        return new CandyBuilder()
                .setColor(Color.valueOf(t.getColor()))
                .setName(t.getName())
                .setPrice((float)t.getPrice())
                .build();
    }
    
    
    
}
