package com.realdolmen.candyshop.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "MnM")
public class MnM extends Candy {

    private Boolean nuts;

    public MnM() {
    }

    public Boolean getNuts() {
        return nuts;
    }

    public void setNuts(Boolean nuts) {
        this.nuts = nuts;
    }
}
