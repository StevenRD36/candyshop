/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.candyshop.mappers;

import com.realdolmen.candyshop.domain.Address;
import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.AddressDTO;
import com.realdolmen.candyshop.dtos.PersonDTO;
import java.util.function.Function;

/**
 *
 * @author SDOAX36
 */
public class PersonDTOMapper implements Function<PersonDTO, Person>{

    @Override
    public Person apply(PersonDTO t) {

        Person p = new Person();
        p.setFirstName(t.getFirstName());
        p.setLastName(t.getLastName());
        p.setBirthDate(t.getBirthDate());

        return p;

    }
    
    
    
}
