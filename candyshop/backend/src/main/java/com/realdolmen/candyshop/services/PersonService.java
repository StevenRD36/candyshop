package com.realdolmen.candyshop.services;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.repository.PersonRepository;

import javax.inject.Inject;
import java.util.List;
import javax.annotation.PostConstruct;

public class PersonService {

    @Inject
    private PersonRepository personRepository;

    public PersonService() {
    }

    @PostConstruct
    public void init(){
        System.out.println("PersonService constructed");
    }
    
    public Person findById(Long id){
        return personRepository.findById(id);
    }
    
    public List<Person> findAllPersons() {
        return personRepository.findAll();
    }

    public List<Person> findPersonByFirstName(String firstName) {
        return personRepository.findPersonsByFirstName(firstName);
    }

    public List<Person> findPersonByLastName(String lastName) {
        return personRepository.findPersonsByLastName(lastName);
    }

    public void addPerson(Person person) {
        personRepository.save(person);
    }
}
