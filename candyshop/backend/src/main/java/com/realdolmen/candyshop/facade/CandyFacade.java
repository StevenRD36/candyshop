
package com.realdolmen.candyshop.facade;

import com.realdolmen.candyshop.domain.Candy;
import com.realdolmen.candyshop.dtos.CandyDTO;
import com.realdolmen.candyshop.mappers.CandyDtoMapper;
import com.realdolmen.candyshop.mappers.CandyMapper;
import com.realdolmen.candyshop.services.CandyService;
import java.io.Serializable;

import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.transaction.Transactional;


/**
 *
 * @author SDOAX36
 */

public class CandyFacade implements Serializable{
    
    @Inject
    private CandyService candyService;
    
    public CandyFacade(){}
    
    public CandyDTO findCandyById(Long id){
        Candy c = candyService.findById(id);
        if(c!=null){
            return new CandyMapper().apply(c);
        }
        return null;
    }
    
    public List<CandyDTO> findAllCandy(){
        List<Candy> candy = candyService.findAll();
        return candy.stream().map(c->new CandyMapper().apply(c)).collect(Collectors.toList());
    }
    
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void addCandy(CandyDTO candyDTO){
        System.out.println("Add candy" + candyDTO.getName());
        Candy c = new CandyDtoMapper().apply(candyDTO);
        System.out.println("Add candy" + c.getName());

         candyService.createCandy(c);
    }
}
