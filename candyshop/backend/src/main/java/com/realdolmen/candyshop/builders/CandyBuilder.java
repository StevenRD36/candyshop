
package com.realdolmen.candyshop.builders;

import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.domain.Candy;

/**
 *
 * @author SDOAX36
 */
public class CandyBuilder {
        private Candy candy;
    
    
    public CandyBuilder() {
        candy = new Candy();
    }

    public CandyBuilder setId(Long id) {
        this.candy.setId(id);
        return this;
    }

    public CandyBuilder setColor(Color color) {
        this.candy.setColor(color);
        return this;
    }

    public CandyBuilder setName(String name) {
        this.candy.setName(name);
        return this;
    }

    public CandyBuilder setPrice(Float price) {
        this.candy.setPrice(price);
        return this;
    }
    
    public Candy build(){
        return candy;
    }
}
