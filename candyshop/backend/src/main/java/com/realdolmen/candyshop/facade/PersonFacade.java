package com.realdolmen.candyshop.facade;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.PersonDTO;
import com.realdolmen.candyshop.mappers.PersonDTOMapper;
import com.realdolmen.candyshop.mappers.PersonMapper;
import com.realdolmen.candyshop.services.PersonService;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

public class PersonFacade implements Serializable {

    @Inject
    private PersonService personService;

    public PersonFacade() {
    }
    
    @PostConstruct
    public void init(){
        System.out.println("PersonFacade constructed");
    }

    public List<PersonDTO> findAllPersons() {
        List<Person> people = personService.findAllPersons();
        return people.stream()
                .map(person -> new PersonMapper().apply(person))
                .sorted((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()))
                .collect(Collectors.toList());
    }
    
    public PersonDTO findById(Long id){
        Person p = personService.findById(id);
        if(p != null){
            return new PersonMapper().apply(p);
        }
        return null;
    }
    
    @Transactional(Transactional.TxType.REQUIRED)
    public void save(PersonDTO dto){
        personService.addPerson(new PersonDTOMapper().apply(dto));
    }
}
