package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.Person;
import java.io.Serializable;

import javax.persistence.EntityManager;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.PersistenceContext;

public class PersonRepository extends AbstractRepository<Person, Long> implements Serializable{
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public PersonRepository() {
        super(Person.class);
    }
    
    @PostConstruct
    public void init(){
//        System.out.println("PersonRepository constructed "+(em == null)+" and transaction "+(transaction == null));
    }

    public PersonRepository(EntityManager em) {
        super(Person.class);
        this.entityManager = em;
    }

    public Long countPerson() {
        return entityManager().createNamedQuery(Person.FIND_COUNT, Long.class).getSingleResult();
    }

    public List<Person> findPersonsByFirstName(String firstname) {
        return entityManager().createNamedQuery(Person.FIND_LIKE_FIRST, Person.class)
                .setParameter("firstname", firstname)
                .getResultList();
    }

    public List<Person> findPersonsByLastName(String lastName) {
        return entityManager().createNamedQuery(Person.FIND_LIKE_LAST, Person.class)
                .setParameter("lastname", lastName)
                .getResultList();
    }


    @Override
    protected EntityManager entityManager() {
        return entityManager;
    }
}
