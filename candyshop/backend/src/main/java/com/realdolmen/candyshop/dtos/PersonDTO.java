package com.realdolmen.candyshop.dtos;

import com.realdolmen.candyshop.domain.Address;
import com.realdolmen.candyshop.domain.Color;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

public class PersonDTO {

    private Long id;
    private String firstName, lastName;
    private LocalDate birthDate;
    private int age;
    private AddressDTO address;

    public PersonDTO(Long id, String firstName, String lastName, LocalDate birthDate, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
