package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.AbstractEntity;
import com.realdolmen.candyshop.domain.Person;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PreDestroy;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

public abstract class AbstractRepository<C, T> {


    protected abstract EntityManager entityManager();

    private Class<C> entityClass;

    public AbstractRepository(Class<C> entity){
        this.entityClass = entity;
    }


    public C findById(T id) {

        return entityManager().find(entityClass,id);
    }
    
    @Transactional
    public void save(C c) {
        if (c != null) {
            try {
                entityManager().persist(c);
            } catch (Exception ex) {
                Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void delete(T id) {
        entityManager().remove(entityManager().find(entityClass, id));
    }


    public List<C> findAll(){
        String clazzz = entityClass.getName();
        return entityManager().createQuery("select c from "+clazzz+" c").getResultList();
    }
    
    @Transactional
    protected void update(C c) {
        if (c != null) {
            try {
                entityManager().merge(c);
                commit();
            } catch (Exception ex) {
                Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void commit() {
        entityManager().getTransaction().commit();
    }

    protected void begin() {
        entityManager().getTransaction().begin();
    }
    
    @PreDestroy
    public void close() {
        if (entityManager() != null) {
            entityManager().close();
        }
    }
}
