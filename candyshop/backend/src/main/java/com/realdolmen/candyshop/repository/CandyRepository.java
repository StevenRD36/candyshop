package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.Candy;
import com.realdolmen.candyshop.domain.Color;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public class CandyRepository extends AbstractRepository<Candy,Long> implements Serializable{
    
    @Produces
    @PersistenceContext
    private EntityManager entityManager;
    
    @PostConstruct
    public void init(){
       System.out.println("PersonRepository constructed "+(entityManager == null));
    }
    

    public CandyRepository(){
        super(Candy.class);
    }
    
    public CandyRepository(EntityManager entityManager){
        super(Candy.class);
        this.entityManager = entityManager;
    }
    
    @Override
    protected EntityManager entityManager() {
        return this.entityManager;
    }
       
}
