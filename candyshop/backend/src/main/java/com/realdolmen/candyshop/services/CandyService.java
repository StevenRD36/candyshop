/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.candyshop.services;

import com.realdolmen.candyshop.domain.Candy;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.repository.CandyRepository;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author SDOAX36
 */
public class CandyService {
    
    @Inject
    private CandyRepository candyRepository;
    

    public CandyService() {
    }
    
    public List<Candy> findAll(){
        return candyRepository.findAll();
    }
    
    public Candy findById(Long id){
        return candyRepository.findById(id);
    }
    
   
    public void createCandy(Candy c){
        Candy cany = new Candy();
        cany.setColor(Color.PINK);
        cany.setName("dslsl");
        cany.setPrice(1.2f);
        candyRepository.save(cany);
    }
}
