
package com.realdolmen.candyshop.dtos.builders;

import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.dtos.CandyDTO;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author SDOAX36
 */
public class CandyDtoBuilder {
    
    private CandyDTO candy;
    
    
    public CandyDtoBuilder() {
        candy = new CandyDTO();
    }

    public CandyDtoBuilder setId(Long id) {
        this.candy.setId(id);
        return this;
    }

    public CandyDtoBuilder setColor(Color color) {
        this.candy.setColor(color);
        return this;
    }

    public CandyDtoBuilder setName(String name) {
        this.candy.setName(name);
        return this;
    }

    public CandyDtoBuilder setPrice(Float price) {
        this.candy.setPrice(price);
        return this;
    }
    
    public CandyDTO build(){
        return candy;
    }
}
