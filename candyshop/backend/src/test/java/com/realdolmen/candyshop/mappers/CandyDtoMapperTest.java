/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.candyshop.mappers;

import com.realdolmen.candyshop.builders.CandyDtoBuilder;
import com.realdolmen.candyshop.domain.Candy;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.dtos.CandyDTO;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author SDOAX36
 */
public class CandyDtoMapperTest {
    
    
    @Test
    public void applyTest(){
        CandyDTO cd = new CandyDtoBuilder()
                .setColor(Color.BLUE)
                .setName("mldjkf")
                .setPrice(2.5f)
                .build();
        
        Candy c = new CandyDtoMapper().apply(cd);
        
        Assert.assertEquals(cd.getColor(),c.getColor().name());        
        Assert.assertEquals(cd.getName(),c.getName());        
        Assert.assertEquals(cd.getPrice(),c.getPrice().intValue());  
    }
}
