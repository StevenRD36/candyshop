package com.realdolmen.candyshop.services;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.repository.PersonRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author SDOAX36
 */
@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {
    
    @Mock
    private PersonRepository personRepository;
    
    @InjectMocks
    private PersonService personService;
    
    private List<Person> persons;
    
    @Before
    public void init(){
        persons = new ArrayList<>();
    }
    
    @Test
    public void findAllPersonsTest(){
        when(personRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(new Person())));
        
        List<Person>result =personService.findAllPersons();
        
        assertEquals(result.size(),1);
        Mockito.verify(personRepository,Mockito.times(1)).findAll();
   
    }
}
