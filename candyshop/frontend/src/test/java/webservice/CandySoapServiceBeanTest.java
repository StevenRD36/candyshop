
package webservice;

import com.realdolmen.candyshop.builders.CandyDtoBuilder;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.dtos.CandyDTO;
import com.realdolmen.candyshop.facade.CandyFacade;
import junit.framework.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;
/**
 *
 * @author SDOAX36
 */

@RunWith(MockitoJUnitRunner.class)
public class CandySoapServiceBeanTest {
    
    @Mock
    private CandyFacade candyFacade;
    @InjectMocks
    private CandySoapServiceBean candySoapServiceBean;
    
    @Ignore
    @Test
    public void addCandyTest(){
        CandyDTO c = new CandyDtoBuilder().setColor(Color.BLACK)
                .setName("Skittles")
                .setPrice(10000f)
                .build();
        
        candySoapServiceBean.addCandy(c);
        verify(candyFacade,times(1)).addCandy(c);
    }
}

