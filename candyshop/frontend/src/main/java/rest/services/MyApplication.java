
package rest.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author SDOAX36
 */
@ApplicationPath("api")
public class MyApplication extends Application{
    
}
