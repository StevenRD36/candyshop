
package rest.services;

import com.realdolmen.candyshop.dtos.PersonDTO;
import com.realdolmen.candyshop.facade.PersonFacade;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author SDOAX36
 */
@Path("person")
public class PersonRestService {
    
    @Inject
    private PersonFacade personFacade;
    
    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllPersons(){
        Persons persons = new Persons(personFacade.findAllPersons());
        return Response.status(Response.Status.OK)
                .entity(persons)
                .build();
    }
    
    @GET
    @Path("{person_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("person_id")Long id){
        PersonDTO p = personFacade.findById(id);
        if(p != null){
            return Response.status(Response.Status.OK)
                    .entity(p)
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .build();
    }
    
    
}
