
package rest.services;

import com.realdolmen.candyshop.dtos.PersonDTO;
import java.util.List;

/**
 * Use only to structure JSON in the REST service
 * @author SDOAX36
 * 
 */
public class Persons {
    
    private List<PersonDTO> persons;
    
    public Persons(List<PersonDTO> persons){
        this.persons = persons;
    }
    
}
