import com.realdolmen.candyshop.dtos.PersonDTO;
import com.realdolmen.candyshop.facade.PersonFacade;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.faces.view.ViewScoped;


@Named
@ViewScoped
public class PersonController implements Serializable {

    @Inject
    private PersonFacade personFacade;
    
    private List<PersonDTO> persons;

    @PostConstruct
    public void init(){
        persons = personFacade.findAllPersons();
    }

    public List<PersonDTO> getPersons() {
        return persons;
    }

    public void setPersons(List<PersonDTO> persons) {
        this.persons = persons;
    }
    
    public void savePerson(){
        PersonDTO person = new PersonDTO(null, "suck", "tkljjml", LocalDate.now(), 0);
        personFacade.save(person);
    }
}
