
package webservice;

import com.realdolmen.candyshop.dtos.CandyDTO;
import com.realdolmen.candyshop.facade.CandyFacade;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 *
 * @author SDOAX36
 */

@WebService(name = "CandyService",portName="CandyServicePort")
public class CandySoapServiceBean implements CandySoapService{

    @Inject
    private CandyFacade candyFacade;
    
    @WebMethod(operationName="GetCandy")
    public @WebResult(name="candy") CandyDTO findById(@WebParam(name= "candy_id") Long id) {
        return candyFacade.findCandyById(id);
    }

    @WebMethod(operationName = "FindCandy")
    public @WebResult(name="candy") List<CandyDTO> findAllCandy() {
        return candyFacade.findAllCandy();
    }

    @WebMethod(operationName = "CreateCandy")
    public void addCandy(@WebParam(name= "candy") CandyDTO candy) {
        System.out.println("Add candy "+candy.getName());
        candyFacade.addCandy(candy);
    }
    
}
