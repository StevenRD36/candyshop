/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import com.realdolmen.candyshop.dtos.CandyDTO;
import java.util.List;

/**
 *
 * @author SDOAX36
 */
public interface CandySoapService {
    CandyDTO findById(Long id);
    List<CandyDTO>findAllCandy();
    void addCandy(CandyDTO candy);
    
}
