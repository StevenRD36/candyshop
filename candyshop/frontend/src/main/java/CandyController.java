
import com.realdolmen.candyshop.builders.CandyDtoBuilder;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.dtos.CandyDTO;
import com.realdolmen.candyshop.facade.CandyFacade;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


@Named
@ViewScoped
public class CandyController implements Serializable{
    
    @Inject
    private CandyFacade candyFacade;
    
    private CandyDTO candy;
    
    private List<CandyDTO> candies;

    public CandyController() {
    }
    
    @PostConstruct
    public void init(){
        candies = candyFacade.findAllCandy();
        candy = new CandyDTO();
    }

    public void saveCandy(){
        candy = new CandyDtoBuilder().setColor(Color.BLUE).setName("tes").setPrice(1.2f).build();
        candyFacade.addCandy(candy);
//        candies = candyFacade.findAllCandy();
    }
    
    public List<CandyDTO> getCandies() {
        return candies;
    }

    public void setCandies(List<CandyDTO> candies) {
        this.candies = candies;
    }

    public CandyDTO getCandy() {
        return candy;
    }

    public void setCandy(CandyDTO candy) {
        this.candy = candy;
    }
    
    
    
    
}
