# Voorbeeld project met 2 modules

## Set-up maven

- Maak een project aan met maven
- Dit zal jouw parent pom worden, verwijder src map en voeg de 2 projectjes toe aan dit project
- in de parent pom moeten de modules toegevoegd worden

````xml

<?xml version="1.0" encoding="UTF-8"?>
<project ... >
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.realdolmen</groupId>
    <artifactId>candyshop</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>
    <modules>
        <module>backend</module>
        <module>frontend</module>
    </modules>
	...
</project>

````
- in de child poms moet er verwezen worden naar de parent

````xml

<?xml version="1.0" encoding="UTF-8"?>
<project ... >
	...
    <parent>
        <artifactId>candyshop</artifactId>
        <groupId>com.realdolmen</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
	...
</project>

````
- bekijk de dependencies goed, elke dependency die in beide modules gebruikt wordt moet in parent pom, anders in de respectievelijke pom
- doe een clean-install van de parent pom
- hopelijk slaagt het

## Module backend

- jar
- bevat de data en business logica
- is een dependency voor de webapp
- Testen!!!

## Module frontend

- war
- bevat controllers en views in jsf
- Testen!!!

## Wildfly config

- Start server
- maak nieuwe user aan in user-cli.bat (bin folder)
	- management user: admin1
	- geef een paswoord mee
	- sla deze gegevens ergens op, bv in readme van wildfly
- localhost:9990 log in met nieuwe user
- voeg jdbc connector toe aan de module
	- vb mysql
	- {wildfly}\modules\system\layers\base\com\mysql\main
	- voeg jar toe aan folder
	- maak module.xml met volgende inhoud:

````xml
<?xml version="1.0"?>
<module name="com.mysql" xmlns="urn:jboss:module:1.5">
	<resources>
		<resource-root path="mysql-connector-java-8.0.12.jar"/>
	</resources>
<dependencies>
	<module name="javax.api"/>
	<module name="javax.transaction.api"/>
</dependencies>
</module>

````
# REST Service

## Find all persons

- Url : 
[/api/person/all](http://localhost:8099/frontend-candyshop/api/person/all)
- Response :
    - persons : ObjectArray
        - address : Object
            - city : String
            - number : String
            - postalCode : String
            - street : String
        - age : int
        - birthDate : Date
        - firstName : String
        - lastName : String
        - id : Long

- example : 

````elixir

"persons" : [{
    "address": {
        "city": "2845",
        "number": "8",
        "postalCode": "London",
        "street": "Harper Lane"
    },
    "age": 0,
    "birthDate": "1970-01-02",
    "firstName": "Jane",
    "id": 2000,
    "lastName": "Doe"
}, 
{}
...
]

````

## Find Person by id

- Url : 
[/api/person/{id}](http://localhost:8099/frontend-candyshop/api/person/1000)
- Param :
    - id : Long
- Response :
    - address : Object
        - city : String
        - number : String
        - postalCode : String
        - street : String
    - age : int
    - birthDate : Date
    - firstName : String
    - lastName : String
    - id : Long

- example : 

````elixir

{
    "address": {
        "city": "2845",
        "number": "8",
        "postalCode": "London",
        "street": "Harper Lane"
    },
    "age": 0,
    "birthDate": "1970-01-02",
    "firstName": "Jane",
    "id": 2000,
    "lastName": "Doe"
}

````
